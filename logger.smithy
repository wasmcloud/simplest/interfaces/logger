// logger.smithy
//

// Tell the code generator how to reference symbols defined in this namespace
metadata package = [ { namespace: "org.example.interfaces.logger", crate: "logger-interface" } ]

namespace org.example.interfaces.logger

use org.wasmcloud.model#wasmbus

/// Description of Logger service
@wasmbus( actorReceive: true )
service Logger {
  version: "0.1",
  operations: [ Display, Ping ]
}

operation Display {
  input: String,
  output: String
}

operation Ping {}

